apt-get update
apt-get install -y  ca-certificates supervisor aria2

mkdir /root/usevps-tmp 
cd /root/usevps-tmp
wget https://github.com/filebrowser/filebrowser/releases/download/v1.9.0/linux-amd64-filebrowser.tar.gz -O linux-amd64-filebrowser.tar.gz
tar -zxf linux-amd64-filebrowser.tar.gz && chmod +x filebrowser
cp filebrowser /usr/local/bin/


mkdir ~/.aria2
mkdir -p /home/Download/aria2

wget https://gitlab.com/usevps/usevps-onekeyscripts/raw/master/aria2/aria2.conf -O aria2.conf && mv aria2.conf ~/.aria2/aria2.conf
wget  https://gitlab.com/usevps/usevps-onekeyscripts/raw/master/aria2/aria2_supervisor.conf -O aria2_supervisor.conf && mv aria2_supervisor.conf /etc/supervisor/conf.d/aria2_supervisor.conf
wget  https://gitlab.com/usevps/usevps-onekeyscripts/raw/master/aria2/filebrowser_supervisor.conf -O filebrowser_supervisor.conf && mv filebrowser_supervisor.conf /etc/supervisor/conf.d/filebrowser_supervisor.conf

supervisorctl reload && sleep 3 && supervisorctl status

